import React from "react";
import ReactDOM from "react-dom/client";
import { createBrowserRouter, RouterProvider } from "react-router-dom";
import "./index.css";
import PageLayout from "./layouts/PageLayout.tsx";
import HomePage from "./pages/HomePage.tsx";
import OlibsPage from "./pages/OlibsPage.tsx";
import ConfigPage from "./pages/ConfigPage.tsx";

const router = createBrowserRouter([
  {
    path: "/",
    element: (
      <PageLayout>
        <HomePage />
      </PageLayout>
    ),
  },
  {
    path: "/olibs",
    element: (
      <PageLayout>
        <OlibsPage />
      </PageLayout>
    ),
  },
  {
    path: "/olibs/config/:id",
    element: (
      <PageLayout>
        <ConfigPage />
      </PageLayout>
    ),
  },
]);

ReactDOM.createRoot(document.getElementById("root")!).render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>
);
