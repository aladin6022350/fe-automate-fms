import Header from "../components/header/Header";
import axios from "axios";
import { useEffect, useState } from "react";
import { IoArrowBack } from "react-icons/io5";
import { useParams, useNavigate } from "react-router-dom";

const ConfigPage = () => {
  let { id } = useParams();
  const [config, setConfig] = useState<{
    id: number;
    module_name: string;
    file_name: string;
  }>();
  const [xlsx, setXlsx] = useState<File>();
  let navigate = useNavigate();

  const onSubmitHandler = async () => {
    try {
      const formData = new FormData();
      formData.append("xlsx", xlsx as Blob);
      if (config?.file_name) {
        formData.append("file_name", config.file_name);
      }
      await axios.post(
        `${import.meta.env.VITE_BASE_URL}/automate_olibs`,
        formData,
        {
          headers: {
            "Content-Type": "multipart/form-data",
          },
        }
      );
    } catch (error) {
      console.log(error);
    }
  };

  const onUploadXLSXHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
    let file = event.target.files?.[0];

    if (file) {
      setXlsx(file);
    }
  };

  const fetchConfig = async () => {
    try {
      const response = await axios.get(
        `${import.meta.env.VITE_BASE_URL}/get_config/${id}`
      );
      setConfig(response.data);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    fetchConfig();
  }, []);

  return (
    <>
      <Header />
      <div
        onClick={() => navigate("/olibs")}
        className="px-32 mt-16 cursor-pointer"
      >
        <IoArrowBack size={32} color={"#1b21cd"} />
      </div>
      <div className="px-16 mt-16 flex justify-center items-center flex-col">
        <h1 className="text-[#1b21cd] text-2xl">Configuration Detail</h1>
        <table>
          <thead>
            <tr>Module</tr>
          </thead>
        </table>
        <ul>
          <li>Module Name : {config?.module_name}</li>
          <li>Configuration : {config?.file_name}</li>
        </ul>

        <label
          className={`bg-[#1b21cd] mt-4 text-white cursor-pointer py-2 px-8 hover:bg-[#474bbc] rounded-lg w-1/4 text-center`}
          htmlFor="xlsx"
        >
          {xlsx ? xlsx.name : "Upload XLSX"}
        </label>
        <input
          className="invisible"
          id="xlsx"
          type="file"
          accept=".xls,.xlsx"
          onChange={(event) => onUploadXLSXHandler(event)}
        />

        {xlsx ? (
          <input
            onClick={() => onSubmitHandler()}
            className={`bg-[#1b21cd] text-white cursor-pointer py-2 px-8 hover:bg-[#474bbc] rounded-lg text-center`}
            type="submit"
            value="Submit"
          />
        ) : (
          ""
        )}
      </div>
    </>
  );
};

export default ConfigPage;
