import { useEffect, useState } from "react";
import Header from "../components/header/Header";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import { FaTrash } from "react-icons/fa";

const OlibsPage = () => {
  const [configs, setConfigs] = useState<
    { id: number; module_name: string; file_name: string }[]
  >([]);
  const [refresh, setRefresh] = useState(false);
  const [uploadedConfig, setUploadedConfig] = useState<File>();
  const [moduleName, setModuleName] = useState("");

  const navigate = useNavigate();

  const onDeleteHandler = async (id: number) => {
    try {
      await axios.delete(
        `${import.meta.env.VITE_BASE_URL}/delete_config/${id}`
      );

      setRefresh(true);
    } catch (error) {
      console.error(error);
    }
  };

  const onUploadConfigHandler = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    const file = event.target.files?.[0];

    if (file) {
      setUploadedConfig(file);
      return;
    }
  };

  const onModuleNameChangeHandler = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setModuleName(event.target.value);
  };

  const onSubmitConfigHandler = async (
    event: React.FormEvent<HTMLFormElement>
  ) => {
    event.preventDefault();
    try {
      const formData = new FormData();
      formData.append("file", uploadedConfig as Blob);
      formData.append("module_name", moduleName.trim());
      await axios.post(
        `${import.meta.env.VITE_BASE_URL}/post_config`,
        formData,
        {
          headers: {
            "Content-Type": "multipart/form-data",
          },
        }
      );
      setRefresh(true);
    } catch (error) {
      console.log(error);
    }
  };

  const fetchConfig = async () => {
    try {
      const response = await axios.get(
        `${import.meta.env.VITE_BASE_URL}/get_configs`
      );

      setConfigs(response.data);
      setRefresh(false);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    fetchConfig();
  }, [refresh]);
  return (
    <>
      <Header />
      <div className="px-16 mt-16 flex justify-center items-center flex-col">
        <form
          onSubmit={(event) => onSubmitConfigHandler(event)}
          className="mt-16 flex justify-center items-center flex-col space-y-4"
        >
          <input
            className="border border-[#1b21cd] rounded-md py-1 px-2"
            type="text"
            maxLength={250}
            required
            placeholder="module name"
            onChange={(event) => {
              onModuleNameChangeHandler(event);
            }}
          />
          <label
            className={`bg-[#1b21cd] text-white cursor-pointer py-2 px-8 hover:bg-[#474bbc] rounded-lg text-center`}
            htmlFor="config"
          >
            {uploadedConfig?.name ? uploadedConfig.name : "Add New Module"}
          </label>

          {uploadedConfig && moduleName && (
            <input
              className={`bg-[#1b21cd] text-white cursor-pointer py-2 px-8 hover:bg-[#474bbc] rounded-lg text-center`}
              type="submit"
              value="Submit"
            />
          )}

          <input
            className="invisible"
            id="config"
            type="file"
            accept=".json"
            onChange={(event) => onUploadConfigHandler(event)}
          />
        </form>
        <h1>Available Modules</h1>

        <div className="relative overflow-x-auto shadow-md sm:rounded-lg">
          <table className="w-full text-sm text-left rtl:text-right text-gray-500 dark:text-gray-400">
            <thead className="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
              <tr>
                <th scope="col" className="px-6 py-3">
                  Module Name
                </th>
                <th scope="col" className="px-6 py-3 text-center">
                  Action
                </th>
              </tr>
            </thead>
            <tbody>
              {configs.map((config, index) => (
                <tr
                  key={index}
                  className="odd:bg-white odd:dark:bg-gray-900 even:bg-gray-50 even:dark:bg-gray-800 border-b dark:border-gray-700"
                >
                  <td className="px-6 py-4">{config.module_name}</td>
                  <td className="px-6 py-4 text-center space-x-2 flex overflow-scroll">
                    <button
                      onClick={() => navigate(`/olibs/config/${config.id}`)}
                      className="cursor-pointer disabled:bg-slate-400 font-medium bg-green-600 hover:bg-green-400 text-white dark:text-white p-2 rounded-lg"
                    >
                      View
                    </button>
                    <button
                      onClick={() => onDeleteHandler(config.id)}
                      className="cursor-pointer disabled:bg-slate-400 font-medium bg-red-600 hover:bg-red-400 text-white dark:text-white p-2 rounded-lg"
                    >
                      <FaTrash />
                    </button>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    </>
  );
};

export default OlibsPage;
