import axios from "axios";
import Header from "../components/header/Header";
import { useState } from "react";
import { ScaleLoader, ClockLoader } from "react-spinners";

const HomePage = () => {
  const [url, setUrl] = useState("");
  const [fileName, setFileName] = useState("");
  const [uploadedFile, setUploadedFile] = useState<File>();
  const [error, setError] = useState("");
  const [isError, setIsError] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [isDownloadable, setIsDownloadable] = useState(false);
  const [isReady, setIsReady] = useState(false);
  const [isAutomating, setIsAutomating] = useState(false);

  const onScrapeHandler = async (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();

    setIsError(false);
    setIsDownloadable(false);
    setIsLoading(true);

    try {
      const response = await axios.post(
        `${import.meta.env.VITE_BASE_URL}/scrape`,
        {
          url: url,
        }
      );

      if (response.status === 200) {
        setIsDownloadable(true);
        setFileName(response.data.fileName);
        setIsLoading(false);
      }
    } catch (err) {
      setIsLoading(false);
      setIsError(true);
      if (axios.isAxiosError(err)) {
        setError(err.response?.data?.message || "An error occurred");
      } else {
        setError("An error occurred");
      }
      console.error(err);
      console.log(error);
    }
  };

  const onDownloadHandler = () => {
    window.location.href = `${
      import.meta.env.VITE_BASE_URL
    }/download/${fileName}`;
  };

  const onUploadFileHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
    setIsReady(false);
    const file = event.target.files?.[0];
    setUploadedFile(file);
    if (file) {
      setIsReady(true);
    }
  };

  const onAutomateHandler = async () => {
    setIsAutomating(true);
    let fileName = uploadedFile?.name.split("_");
    if (fileName) {
      let url = `${fileName[0]}://${fileName[1]}/${fileName[2]}`;
      try {
        const formData = new FormData();
        formData.append("cy_test", "calculator");
        formData.append("url", url);
        formData.append("file", uploadedFile as Blob);

        await axios.post(
          `${import.meta.env.VITE_BASE_URL}/automate`,
          formData,
          {
            headers: {
              "Content-Type": "multipart/form-data",
            },
          }
        );
        setIsAutomating(false);
      } catch (err) {
        console.error(err);
        setIsAutomating(false);
      }
    }
  };

  return (
    <>
      <Header />
      <div className="flex justify-center mt-16 flex-col">
        <div className="flex flex-col items-center">
          <form className="mb-8" onSubmit={(event) => onScrapeHandler(event)}>
            <input
              type="text"
              className="border border-[#1b21cd]  rounded-lg pt-1 pr-10 pb-1 pl-4 w-96 hover:bg-slate-100 focus:bg-slate-100"
              placeholder="Enter URL To Scrape..."
              value={url}
              disabled={isLoading || isAutomating}
              onChange={(event) => setUrl(event.target.value)}
              required
            />
            <button className="relative right-6 ">
              <span
                className={`${
                  isLoading || isAutomating ? "bg-slate-500" : "bg-[#1b21cd]"
                } hover:bg-[#474bbc] text-white py-[0.483rem] px-4 rounded-r-lg`}
              >
                Scrape
              </span>
            </button>
          </form>
          {isError && <p className="text-red-500 text-center">{error}</p>}
          {isLoading && <ScaleLoader color="#1b21cd" />}

          {isDownloadable && (
            <a
              onClick={() => onDownloadHandler()}
              className="bg-[#1b21cd] text-white cursor-pointer py-2 px-8 hover:bg-[#474bbc] rounded-lg"
            >
              Download XLSX
            </a>
          )}
        </div>

        <div className="flex flex-col items-center ">
          <h1 className="text-[#1b21cd] font-bold mt-8">UPLOAD XLSX</h1>
          <label
            className={`${
              isAutomating ? "bg-slate-500" : "bg-[#1b21cd]"
            } text-white cursor-pointer py-2 px-8 hover:bg-[#474bbc] rounded-lg w-1/4 text-center`}
            htmlFor="xlsx"
          >
            {uploadedFile?.name || "Upload XLSX"}
          </label>
          <input
            className="invisible"
            id="xlsx"
            type="file"
            accept=".xls,.xlsx"
            disabled={isAutomating}
            onChange={(event) => onUploadFileHandler(event)}
          />

          {isReady && (
            <input
              onClick={() => onAutomateHandler()}
              className="bg-[#1b21cd] disabled:bg-slate-500  text-white cursor-pointer py-2 px-8 hover:bg-[#474bbc] rounded-lg w-1/4 text-center"
              type="button"
              disabled={isAutomating}
              value="Automate!!!"
            />
          )}

          {isAutomating && <ClockLoader className="mt-8" color="#1b21cd" />}
        </div>
      </div>
    </>
  );
};

export default HomePage;
