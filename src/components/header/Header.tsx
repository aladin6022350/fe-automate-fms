import { useNavigate } from "react-router-dom";

const Header = () => {
  const navigation = useNavigate();
  return (
    <div className="py-4 bg-[#1b21cd] text-white px-32">
      <ul className="flex items-center space-x-10">
        <li
          onClick={() => navigation("/")}
          className="text-2xl font-bold cursor-pointer"
        >
          RPA
        </li>
        <li
          onClick={() => navigation("/")}
          className="cursor-pointer hover:bg-[#474bbc] p-2 hover:rounded"
        >
          Generic Web Scraper
        </li>
        <li
          onClick={() => navigation("/olibs")}
          className="cursor-pointer hover:bg-[#474bbc] p-2 hover:rounded"
        >
          Olibs Automate
        </li>
      </ul>
    </div>
  );
};

export default Header;
